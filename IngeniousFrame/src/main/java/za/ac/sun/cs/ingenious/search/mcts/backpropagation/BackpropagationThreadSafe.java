package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.core.util.search.TreeNode;

public interface BackpropagationThreadSafe<N extends TreeNode<?, ?, ?>> {

	/**
	 * Update the fields relating to this backpropagation strategy for the current node.
	 *
	 * @param node
	 * @param results the win/loss or draw outcome for the simulation relating to the current playout.
	 */
	void propagate(N node, double[] results);
}
