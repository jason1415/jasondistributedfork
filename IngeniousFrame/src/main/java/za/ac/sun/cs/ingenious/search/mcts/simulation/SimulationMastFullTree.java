package za.ac.sun.cs.ingenious.search.mcts.simulation;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimulationMastFullTree<S extends GameState> implements SimulationThreadSafe<S> {

    GameLogic<S> logic;
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    ArrayList<Action> moves = new ArrayList<>();

    private MastTable visitedMovesTable;
    private double tau;
    boolean recordMoves;


    /** constructor used by the game engine which specifies parameters */
    public SimulationMastFullTree(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs, MastTable visitedMovesTable, double tau, boolean recordMoves) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.visitedMovesTable = visitedMovesTable;
        this.tau = tau;
        this.recordMoves = recordMoves;
    }

    /**
     * The method that performs random simulation playout.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(S state) {
        ArrayList<Action> moveList = new ArrayList<>();

        while (!logic.isTerminal(state)) {
            int playerToPlay = -1;
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                playerToPlay = playerId;
            }

            List<Action> actions = logic.generateActions(state, playerToPlay);

            Action nextAction;
            nextAction = getNextAction(state, actions);
            if (recordMoves) {
                moves.add(nextAction);
            }
            logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerToPlay));
            moveList.add(nextAction);
        }

        double[] scores = evaluator.getMctsScore(state);
        if (moveList.size() >= 1) {
            updateVisitedMovesTable(moveList, scores);
        }
        SimulationTuple returnValues = new SimulationTuple(moves, scores);
        return returnValues;
    }

    /**
     * @param state
     * @return the next action as chosen by the MAST algorithm
     */
    public Action getNextAction(S state, List<Action> possibleActions) {
        Random randomGen = new Random();
        double bestGibbsValueSoFar = 0;
        MastTable.StoredAction bestAction = null;
        // if actions are possible, choose one randomly
        if (!possibleActions.isEmpty()) {
            int randomNumber;

            Action actionChoice;

            // stochastic distribution to prevent looping
            if (randomGen.nextInt(100) > 90) {
                randomNumber = randomGen.nextInt(possibleActions.size());
                Action randomChoice = possibleActions.get(randomNumber);
                actionChoice = randomChoice;
            } else {
                double denominator = visitedMovesTable.getWinToVisitValues();

                for (int i = 0; i < possibleActions.size(); i++) {
                    MastTable.StoredAction currentAction = visitedMovesTable.getMoveCombinationScores().get(possibleActions.get(i));
                    double numerator;
                    double currentGibbsValue;

                    if (possibleActions.get(i).getPlayerID() != -1) {
                        if (currentAction != null) {
                            numerator = Math.exp((currentAction.getWins()/currentAction.getVisitCount())/tau);
                            currentGibbsValue = numerator/denominator;
                            if (currentGibbsValue > bestGibbsValueSoFar) {
                                bestGibbsValueSoFar = currentGibbsValue;
                                bestAction = currentAction;
                            }
                        } else if (currentAction == null) {
                            bestAction = visitedMovesTable.createNewStoredAction(possibleActions.get(i));
                            break;
                        }

                    }
                }

                if (bestAction == null) {
                    randomNumber = randomGen.nextInt(possibleActions.size());
                    actionChoice = possibleActions.get(randomNumber);
                } else {
                    actionChoice = bestAction.getAction();
                }
            }
            return actionChoice;
        }
        Log.error("SimulationRandom", "Error during simulation: state is not terminal, however there is also no actions possible");
        Log.error("SimulationRandom", ("PROBLEM STATE: \n" + state + "\nIs PROBLEM STATE terminal? = " + logic.isTerminal(state)));
        return null;
    }

    /**
     * backpropagation phase for the simulation phase in which values in the mast table are updated
     * with simulation move information
     * @param moveList
     * @param scores
     */
    public void updateVisitedMovesTable(ArrayList<Action> moveList, double[] scores) {
        for (int i = 0; i < moveList.size(); i++) {
            Action firstAction = moveList.get(i);
            boolean win = false;
            if (firstAction.getPlayerID() != -1) {
                if (scores[firstAction.getPlayerID()] == 1) {
                    win = true;
                }
                MastTable.StoredAction firstActionFromTable = visitedMovesTable.getMoveCombinationScores().get(firstAction);

                if (firstActionFromTable != null) {
                    // Update the score of node action
                    double oldValue = Math.exp((firstActionFromTable.getWins() / firstActionFromTable.getVisitCount()) / visitedMovesTable.getTau());
                    firstActionFromTable.incVisitCount();
                    if (win) {
                        firstActionFromTable.incWins();
                    }
                    double newValue = Math.exp((firstActionFromTable.getWins() / firstActionFromTable.getVisitCount()) / visitedMovesTable.getTau());
                    visitedMovesTable.decWinToVisitValues(oldValue);
                    visitedMovesTable.incWinToVisitValues(newValue);
                } else {
                    // Add new action to table
                    MastTable.StoredAction newStoredAction = visitedMovesTable.createNewStoredAction(firstAction);
                    newStoredAction.incVisitCount();
                    if (win) {
                        newStoredAction.incWins();
                    }

                    visitedMovesTable.getMoveCombinationScores().put(newStoredAction.getAction(), newStoredAction);
                    visitedMovesTable.incWinToVisitValues(Math.exp((newStoredAction.getWins() / newStoredAction.getVisitCount()) / visitedMovesTable.getTau()));
                }
            }
        }
    }

    /**
     * @return the list of moves made during the simulation
     */
    public ArrayList<Action> getMoves() {
        return moves;
    }

    /**
     * Game logic object getter.
     *
     * @return The game logic
     */
    public GameLogic<S> getLogic() {
        return logic;
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationMastFullTree(GameLogic<SS> logic, MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, MastTable visitedMovesTable, double tau, boolean recordMoves) {
        return new SimulationMastFullTree(logic, evaluator, obs, visitedMovesTable, tau, recordMoves);
    }

}
