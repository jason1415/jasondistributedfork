thread="1"
turnTime="3000"
> $1"_"$2"_"$turnTime"_"$thread".out"
echo "Turn Length: " $turnTime >> $1"_"$2"_"$turnTime"_"$thread".out"
echo "Thread Count: " $thread >> $1"_"$2"_"$turnTime"_"$thread".out"
echo "Latest Commit:  Adjusted MCTS Backpropagation RAVE strategy to enhanced value estimation function" >> $1"_"$2"_"$turnTime"_"$thread".out"
echo "Comparing " $1 " against " $2 ":"
for i in `seq 1 $3`
do
    ./connect_clients.sh 4 $1 $2 | gawk -v p1=$1 -v p2=$2  -f parse_server_data.awk >> $1"_"$2"_"$turnTime"_"$thread".out"
	echo "Done with " $i "out of " $3
	echo "---" >> $1"_"$2"_"$turnTime"_"$thread".out"
done
