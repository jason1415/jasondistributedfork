package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.network.Message;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;

/**
 * Super class for all messages that are used to initialize the game. Extend this to
 * supply game specific information (e.g. sending the initial game state, etc).
 * A possible extension of this is {@link MatchSettingsInitGameMessage}
 * 
 * @author Stephan Tietz
 */
public class InitGameMessage extends Message {

	private static final long serialVersionUID = 1L;

}
