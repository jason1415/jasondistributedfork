package za.ac.sun.cs.ingenious.core.commandline;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.ParameterException;
import com.esotericsoftware.minlog.Log;

/**
 * Created by Chris Coetzee on 2016/07/27.
 */
public abstract class BaseArguments {

    protected JCommander getJcommanderInstance() {
        JCommander commander = new JCommander(this);
        return commander;
    }

    public void processArguments(String[] args) {
        /* Parse the commandline arguments */

        JCommander commander = getJcommanderInstance();
        try {
            commander.parse(args);
        } catch (MissingCommandException e) {
            Log.error("Unknown command. Run jar to see valid commands.");
            //commander.usage();
            System.exit(1);
        } catch (ParameterException e) {
            //e.printStackTrace();
            if (commander.getParsedCommand() != null) {
                commander.usage(commander.getParsedCommand());
            } else {
                commander.usage();
            }
            System.exit(1);
        }

        try {
            validateAndPrepareArguments(commander);
        } catch (IllegalArgumentException e) {
            System.exit(2);
        }
    }

    /**
     * Performs any additional checks on the arguments. It should print out an error, and raise an
     * IllegalArgumentException if some argument is incorrect.
     */
    public abstract void validateAndPrepareArguments(JCommander commander);
}
