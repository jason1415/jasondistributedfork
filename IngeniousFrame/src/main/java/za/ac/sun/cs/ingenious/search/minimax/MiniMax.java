package za.ac.sun.cs.ingenious.search.minimax;

import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * This implements a minimax search procedure with alpha-beta pruning for 2-player turn based games
 * TODO drop restriction to 2-player games, see issue #214
 * @author Michael Krause
 * @author steven
 */
public class MiniMax<S extends TurnBasedGameState, L extends GameLogic<S>, E extends GameEvaluator<S>> {

	private int maxSearchDepth;
	private E eval;
	private int currentPlayerID;
	private L logic;
	
	/**
	 * Creates a Minimax search object
	 * 
	 * @param maxSearchDepth Max depth to descend to in the search TreeEngine. Once the max depth or a terminal state has been reached, the evaluator is used to compute a (heuristic) value for the state. By setting this to a negative value like -1, one ensures that the TreeEngine is always evaluated completely.
	 * @param eval Evaluator for non-terminal states
	 * @param logic Game logic object
	 * @param playerID Player to execute the MiniMax search for
	 */
	public MiniMax(int maxSearchDepth, E eval, L logic, int playerID) {
		this.maxSearchDepth = maxSearchDepth;
		this.eval = eval;
		this.logic = logic;
		this.currentPlayerID = playerID;
	}
	
	/**
	 * @param start Some state in the game
	 * @return The best action to take from start for the playerID set in the constructor.
	 */
	public Action generateMove (S start) {
		List<Action> actions = orderMoves(logic.generateActions(start,start.nextMovePlayerID), start);
		Action bestMove = actions.get(0);
		double bestValue = Double.NEGATIVE_INFINITY;
		double alpha = Double.NEGATIVE_INFINITY;
		double beta = Double.POSITIVE_INFINITY;
		
		for (Action child : actions) {
			S clonedState = (S) start.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MinMove(clonedState, alpha, beta, 0);			

			if (bestValue < value) {
				bestValue = value;
				bestMove = child;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(alpha >= beta){
				break;
			}
		}

		return bestMove;
	}
	
	private double MaxMove(S state,double alpha,double beta, int depth) {

		if (logic.isTerminal(state) || this.maxSearchDepth == depth) {
			return eval.getScore(state)[currentPlayerID];
		}

		double bestValue = Double.NEGATIVE_INFINITY;

		List<Action> actions = orderMoves(logic.generateActions(state,state.nextMovePlayerID), state);
		for (Action child : actions) {
			S clonedState = (S) state.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MinMove(clonedState, alpha, beta, depth +1);

			if (bestValue < value) {
				bestValue = value;
			}
			
			alpha = Math.max(alpha, bestValue);
			if(alpha >=beta){
				break;
			}
		}

		return bestValue;
	}

	private double MinMove(S state,double alpha,double beta, int depth) {

		if (logic.isTerminal(state) || this.maxSearchDepth == depth) {
			return eval.getScore(state)[currentPlayerID];
		}

		double bestValue = Double.POSITIVE_INFINITY;

		List<Action> actions = orderMoves(logic.generateActions(state,state.nextMovePlayerID), state);
		for (Action child : actions) {
			S clonedState = (S) state.deepCopy();
			logic.makeMove(clonedState, child);
			double value = MaxMove(clonedState, alpha, beta, depth +1);

			if (bestValue > value) {
				bestValue = value;
			}
			
			beta = Math.min(beta, bestValue);
			if(beta <=alpha){
				break;
			}
		}

		return bestValue;
	}
	
	/**
	 * @param actions List of actions possible at the given state for the player to act.
	 * @param state Current state during the search
	 * @return An ordering of the actions. For MiniMax search it can sometimes be beneficial to change the order of the actions to be searched. This can be achieved by overriding this method. Per default, the input list of actions is returned unchanged.
	 */
	protected List<Action> orderMoves(List<Action> actions, S state) {
		/*
		 * default move ordering is random
		 */
		return actions;
	}

}
