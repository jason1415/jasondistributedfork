package za.ac.sun.cs.ingenious.core.network.lobby.messages;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * This message is sent by the GameServer as part of the handshake with the client that
 * occurs when a client has successfully joined a lobby on the server. It contains the ID
 * assigned to the newly connected player.
 * 
 * @author Michael Krause
 */
public class JoinedLobbyMessage extends Message {

	private static final long serialVersionUID = 1L;
	private final int playerID;
	
	public JoinedLobbyMessage(int playerID) {
		this.playerID = playerID;
	}
	
	public int getPlayerID() {
		return playerID;
	}

}
