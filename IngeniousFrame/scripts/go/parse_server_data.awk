BEGIN {
    p1_count = 0
    p2_count = 0
    score_count = 0
}
{
	if ($0 ~ p1 || $0 ~ p2) {
		print "one result" split($0, a, ": ")
		for (i = 1; i < split($0,a,": "); i++) {
			if ($i == p1) {
				p1_score = $(i+2)
				score_count = score_count + 1
			}
			if ($i == p2) {
				p2_score = $(i+2)
				score_count = score_count + 1
			}
		}	
	}
    if (p1_score > p2_score && score_count == 2){
        p1_count = p1_count + 1
		p1_score = 0
		p2_score = 0
		score_count = 0
    }
    if (p1_score < p2_score && score_count == 2) {
        p2_count = p2_count + 1
		p1_score = 0
		p2_score = 0
		score_count = 0
    }	
}
END {
    print p1 " : " p1_count " - " (p1_count/(p1_count + p2_count))*100 "%"
    print p2 " : " p2_count " - " (p2_count/(p1_count + p2_count))*100 "%"
}
