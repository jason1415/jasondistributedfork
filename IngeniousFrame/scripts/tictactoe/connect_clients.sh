#!/bin/bash
# Here we start two clients
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client

# Simple enhancement options include:
# Vanilla
# Rave
# Mast
# Contextual
# FPU
# FPUTuned

#Combination enhancement options include
# RaveMast
# RaveContextual
# MastFPU
# MastFPUTuned
# FPUContextual
# FPUTunedContextual

for i in `seq 1 $1`
do
  # creates a new tictactoe game with the default configuration
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "TicTacToe.json" -game "TTTReferee" -lobby "mylobby" -port $4
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$2_player1" -config "EnhancementChoices/EnhancementChoice$2.json" -threadCount 1 -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.tree.TTTMCTSTreeGenericEngine" -game "TTTReferee" -hostname localhost -port $4 &
  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$3_player2" -config "EnhancementChoices/EnhancementChoice$3.json" -threadCount 1 -engine "za.ac.sun.cs.ingenious.games.tictactoe.engines.tree.TTTMCTSTreeGenericEngine" -game "TTTReferee" -hostname 127.0.0.1 -port $4
  echo "-" -n
done
