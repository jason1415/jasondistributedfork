package za.ac.sun.cs.ingenious.core.util;

import com.esotericsoftware.minlog.Log;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by Chris Coetzee on 2016/07/29.
 */
public class ClassUtils {

    /**
     * Returns the directory relative to which this class is resolved
     */
    public static File getTopLevelDirectory(Class<?> referenceClass) throws URISyntaxException {
        final URL url = referenceClass.getProtectionDomain().getCodeSource().getLocation();
        final File pathToFile = new File(url.toURI()).getAbsoluteFile();
        return pathToFile;
    }

    public static void main(String[] args) throws Exception {
        Log.info(getTopLevelDirectory(ClassUtils.class));
    }
}
