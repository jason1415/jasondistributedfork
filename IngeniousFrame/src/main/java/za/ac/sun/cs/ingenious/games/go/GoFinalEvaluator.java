package za.ac.sun.cs.ingenious.games.go;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

import java.util.Arrays;

public class GoFinalEvaluator implements GameEvaluator<TurnBasedSquareBoard> {

    @Override
    public double[] getScore(TurnBasedSquareBoard forState) {
        double[] scores = new double[2];
        for (int i = 0; i < forState.getBoardSize(); i++) {
            for (int j = 0; j < forState.getBoardSize(); j++) {
                if (forState.board[forState.xyToIndex(i, j)] == 0) {
                    double[] answer = areaScoring(forState, new XYAction(i, j, 0), new double[2]);
                    if (answer[0] != -1) {
                        scores[(int)answer[0] - 1] = scores[(int)answer[0] - 1] + answer[1];
                    }
                } else if (forState.board[forState.xyToIndex(i, j)] != 3){
                    scores[forState.board[forState.xyToIndex(i, j)] - 1]++;
                }
            }
        }
        if (scores == null) {
            Log.info("Hit2 -- FinalEvaluator");
        }
        return scores;
    }

    private double[] areaScoring(TurnBasedSquareBoard fromState, XYAction move, double[] owner) {
        int x = move.getX();
        int y = move.getY();
        boolean control = true;
        byte[] storeBoard = Arrays.copyOf(fromState.board, fromState.board.length);
        fromState.board[fromState.xyToIndex(x, y)] = 3;
        if (x + 1 < fromState.getBoardSize()) {
            if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x + 1, y)] != 3) {
                owner[0] = fromState.board[fromState.xyToIndex(x + 1, y)];
            }
            if (fromState.board[fromState.xyToIndex(x + 1, y)] == 0) {
                owner = areaScoring(fromState, new XYAction(x + 1, y, 0), owner);
            } else if (fromState.board[fromState.xyToIndex(x + 1, y)] != owner[0] && fromState.board[fromState.xyToIndex(x + 1, y)] != 3) {
                control = false;
            }
        }
        if (x - 1 >= 0) {
            if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x - 1, y)] != 3) {
                owner[0] = fromState.board[fromState.xyToIndex(x - 1, y)];
            }
            if (fromState.board[fromState.xyToIndex(x - 1, y)] == 0) {
                owner = areaScoring(fromState, new XYAction(x - 1, y, 0), owner);
            } else if (fromState.board[fromState.xyToIndex(x - 1, y)] != owner[0] && fromState.board[fromState.xyToIndex(x - 1, y)] != 3) {
                control = false;
            }
        }
        if (y + 1 < fromState.getBoardSize()) {
            if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x, y + 1)] != 3) {
                owner[0] = fromState.board[fromState.xyToIndex(x, y + 1)];
            }
            if (fromState.board[fromState.xyToIndex(x, y + 1)] == 0) {
                owner = areaScoring(fromState, new XYAction(x, y + 1, 0), owner);
            } else if (fromState.board[fromState.xyToIndex(x, y + 1)] != owner[0] && fromState.board[fromState.xyToIndex(x, y + 1)] != 3) {
                control = false;
            }
        }
        if (y - 1 >= 0) {
            if (owner[0] == 0 && fromState.board[fromState.xyToIndex(x, y - 1)] != 3) {
                owner[0] = fromState.board[fromState.xyToIndex(x, y - 1)];
            }
            if (fromState.board[fromState.xyToIndex(x, y - 1)] == 0) {
                owner = areaScoring(fromState, new XYAction(x, y - 1, 0), owner);
            } else if (fromState.board[fromState.xyToIndex(x, y - 1)] != owner[0] && fromState.board[fromState.xyToIndex(x, y - 1)] != 3) {
                control = false;
            }
        }
        if (control) {
            owner[1]++;
        } else {
            owner[0] = -1;
        }
        fromState.board = Arrays.copyOf(storeBoard, fromState.board.length);
        return owner;
    }

}
