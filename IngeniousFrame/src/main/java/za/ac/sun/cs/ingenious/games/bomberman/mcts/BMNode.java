package za.ac.sun.cs.ingenious.games.bomberman.mcts;


// TODO Add proper MCTS for Bomberman, see issue #238 

//public class BMNode extends SearchNode<BMBoard,BMNode> {
//
//    private BMLogic bmlogic;
//    private short currentPlayer;
//
//	public BMNode(BMBoard board, short currentPlayer){
//		this(board, null, null, currentPlayer);
//	}
//	
//	public BMNode(BMBoard board, Action toThisMove, BMNode parent, short currentPlayer) {
//		super(board, BMLogic.defaultBMLogic, toThisMove, parent);
//        this.currentPlayer = currentPlayer;
//        try {
//    		bmlogic = (BMLogic) logic;
//        } catch (ClassCastException e) {
//            Log.error("Invalid logic used for BMNode, defaulting to default BM logic");
//            bmlogic = BMLogic.defaultBMLogic;
//        }
//	}	
//	
//	@Override
//	public int getCurrentPlayer() {
//		return currentPlayer;
//	}
//
//	@Override
//	public String toString() {
//		currentState.printPretty();
//		if (parentToThisMove != null) {
//			return depth + " " + parentToThisMove.toString() + "		" + rewards + "/" + visits;
//		} else {
//			return depth + " null	" + rewards + "/" + visits;
//		}
//	}
//
//	@Override
//	public BMNode expandAChild(Move childMove) {
//		BMBoard nextBoard = (BMBoard) getGameState().deepCopy();
//		short nextPlayer = (short)((currentPlayer+1)%4);
//		if(nextPlayer == 0){
//			bmlogic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), nextBoard);
//		}
//		return new BMNode(nextBoard, (Action) childMove, this, nextPlayer);
//	}
//}
