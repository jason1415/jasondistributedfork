package za.ac.sun.cs.ingenious.core.util.state;

import com.esotericsoftware.minlog.Log;

import java.util.Arrays;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Represents the state of a game that is played on a board with any two dimensions where
 * players take turns. Useful for games like MNK.
 */
public class TurnBased2DBoard extends TurnBasedGameState {

	public int[][] board;
	protected int height;
	protected int width;

	/**
	 * Initializes an empty board.
	 */
	public TurnBased2DBoard(int height, int width, int firstPlayer, int numPlayers) {
		super(firstPlayer, numPlayers);
		this.height = height;
		this.width = width;
		this.board = new int[height][width];
	}

	/**
	 * Copy constructor. Duplicates the given board.
	 */
	public TurnBased2DBoard(TurnBased2DBoard toCopy) {
		super(toCopy.nextMovePlayerID, toCopy.numPlayers);
		this.height = toCopy.getHeight();
		this.width = toCopy.getWidth();
		this.board = new int[this.height][this.width];
		for (int h = 0; h < this.height; h++) {
			for (int w = 0; w < this.width; w++) {
				this.board[h][w] = toCopy.board[h][w];
			}
		}
	}

	@Override
	public GameState deepCopy() {
		return new TurnBased2DBoard(this);
	}

	public int getHeight() {
		return this.height;
	}

	public int getWidth() {
		return this.width;
	}

	@Override
	public void printPretty() {
		StringBuilder s = new StringBuilder();
		s.append("\n+");
		for (int i = 0; i < this.width; i++) {
			s.append("-");
		}
		s.append("+\n");

		for (int i = 0; i < this.height; i++) {
			s.append("|");
			for (int j = 0; j < this.width; j++) {
				s.append(this.board[i][j]);
			}
			s.append("|\n");
		}

		s.append("+");
		for (int i = 0; i < this.width; i++) {
			s.append("-");
		}
		s.append("+ \n");
		Log.info(s);

	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.deepHashCode(board);
		result = prime * result + height;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurnBased2DBoard other = (TurnBased2DBoard) obj;
		if (height != other.height)
			return false;
		if (width != other.width)
			return false;
		if (!Arrays.deepEquals(board, other.board))
			return false;
		return true;
	}

}
