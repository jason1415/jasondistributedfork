package za.ac.sun.cs.ingenious.games.othello.gamestate;

public enum GameFinalState {
	BLACK_WIN(0),
	WHITE_WIN(1),
	DRAW(2),
	NOT_TERMINAL(3),
	FAILURE(4); // In case the game was stopped due to some error

	public final int value;

	private GameFinalState(int value) {
		this.value = value;	
	}

	public int value() {
		return value;
	}
}
