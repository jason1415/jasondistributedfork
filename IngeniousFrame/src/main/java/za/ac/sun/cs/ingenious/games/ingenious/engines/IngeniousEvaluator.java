package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;

public abstract class IngeniousEvaluator {

	public double evaluate(IngeniousScoreKeeper scores,IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> gameBoard, ArrayList<IngeniousRack> racks ,int playerId){
		double eval = 0;
		int[] score = scores.getScore(playerId).getScore();
		int[] moveScore = scores.calculateScore(gameBoard);
		
		
		for(int i = 0; i< score.length;i++){
			eval += moveScore[i]/(double)score[i];
		}
		return eval;
	}



}
