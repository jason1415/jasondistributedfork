package za.ac.sun.cs.ingenious.games.mnk.moves;

import za.ac.sun.cs.ingenious.core.util.move.XYAction;

/**
 * A XYAction indicating that the given coordinates are already occupied by
 * another player
 */
public class OccupiedXYAction extends XYAction {

	private static final long serialVersionUID = 1L;

	private int uncoveringPlayer;

	public OccupiedXYAction(int x, int y, int player, int uncoveringPlayer) {
		super(x, y, player);
		this.uncoveringPlayer = uncoveringPlayer;
	}

	public int getUncoveringPlayer() {
		return uncoveringPlayer;
	}

}
