package za.ac.sun.cs.ingenious.games.bomberman.mcts;

import za.ac.sun.cs.ingenious.search.mcts.legacy.SearchNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;

public class BMUpdater extends SimpleUpdater {

	@Override
	public void backupValue(SearchNode node, double[] result) {
		int currentPlayer = node.getCurrentPlayer();
		SearchNode iterator = node;
		while (iterator != null) {
			if((currentPlayer+1)%4  == iterator.getCurrentPlayer())
				iterator.addReward(result);
			else
				iterator.addReward(new double[result.length]);
			iterator = iterator.getParent();
		}
	}

}
