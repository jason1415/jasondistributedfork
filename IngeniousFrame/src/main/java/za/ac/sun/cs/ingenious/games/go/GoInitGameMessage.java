package za.ac.sun.cs.ingenious.games.go;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * @author Michael Krause
 */
public class GoInitGameMessage extends InitGameMessage {

	private static final long serialVersionUID = 1L;

	private int boardSize;

	public GoInitGameMessage(int boardSize) {
		this.boardSize = boardSize;
	}
	
	public int getBoardSize() {
		return boardSize;
	} 
}
