package za.ac.sun.cs.ingenious.core.util.message;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;

/**
 * An {@link InitGameMessage} containing the {@link MatchSetting}s of this game. Note that the
 * match settings may sometimes contain information that (some) players should not see. In
 * those cases, a custom {@link InitGameMessage} should be used that is initialized with the
 * necessary information only.
 */
public class MatchSettingsInitGameMessage extends InitGameMessage {

	private static final long serialVersionUID = 1L;

	private final MatchSetting settings;

	public MatchSettingsInitGameMessage(MatchSetting settings) {
		this.settings = settings;
	}

	public MatchSetting getSettings() {
		return settings;
	}

}
