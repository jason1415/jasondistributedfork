package za.ac.sun.cs.ingenious.games.othello;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;

import za.ac.sun.cs.ingenious.games.othello.gamestate.*;
import za.ac.sun.cs.ingenious.games.othello.network.*;
import za.ac.sun.cs.ingenious.games.othello.engines.OthelloEngine;

/**
 * A referee for the Othello (Reversi) board game. 
 * The referee handles the communication between players and terminates
 * the game if it is complete or if the clock of any player runs out.
 */
public class OthelloReferee 
	extends FullyObservableMovesReferee<OthelloBoard, OthelloLogic, OthelloFinalEvaluator> {

	/**
	 * Constructs a new referee that handles lobby and game logic.
	 */
	public OthelloReferee(MatchSetting match, PlayerRepresentation[] players) 
		throws IncorrectSettingTypeException {

		super(match, players, new OthelloBoard(), OthelloLogic.defaultOthelloLogic,
			  new OthelloFinalEvaluator());

		/* TODO: enable when TUI is complete
        if (match.getSettingAsBoolean("uiOn", true)) {
			new OthelloTUI(this);
        }
		*/
	}
	
	@Override
	protected void beforeGameStarts() {
		updateUI();
	}
	
	@Override
	protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
		return new OthelloInitGameMessage((byte) player.getID());
	}

	@Override
	protected GameTerminatedMessage createTerminateGameMessage(PlayerRepresentation player) {
		return new OthelloGameTerminatedMessage(eval.getScore(currentState));
	}

	@Override
	protected void updateServerState(PlayActionMessage m) {
		super.updateServerState(m);
		updateUI(m.getAction());
	}

	@Override
	protected void afterGameTerminated() {
		super.afterGameTerminated();

		/* TODO: draw game stats in UI */
	}

	/**
	 * Draws all UI components.
	 */
	private void updateUI() {
		/* TODO */
	}

	/**
	 * Updates the UI to reflect an action performed by a player.
	 *
	 * @param action	the action performed by a player
	 */
	private void updateUI(Action action) {
		if (action == null) {
			return;
		}

		/* TODO: add TUI stuff */

	}
}
