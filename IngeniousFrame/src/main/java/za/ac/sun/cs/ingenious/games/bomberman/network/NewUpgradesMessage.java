package za.ac.sun.cs.ingenious.games.bomberman.network;

import java.awt.Point;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

public class NewUpgradesMessage extends PlayedMoveMessage {

	private static final long serialVersionUID = 1L;
	private Iterable<Point> blastUpgrades;
	private Iterable<Point> bombUpgrades;
	
	/**
	 * Message containing all visible upgrades on the map.
	 *
	 * @param blastUpgrades
	 * @param bombUpgrades
	 */
	public NewUpgradesMessage(Iterable<Point> blastUpgrades, Iterable<Point> bombUpgrades) {
		super((Move) null);
		this.blastUpgrades = blastUpgrades;
		this.bombUpgrades = bombUpgrades;
	}
	
	public Iterable<Point> getBlastUpgrades() {
		return blastUpgrades;
	}
	
	public Iterable<Point> getBombUpgrades() {
		return bombUpgrades;
	}
}
