package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionInterface;

public class MctsCMCNodeExtensionComposition<S extends GameState, C, P> implements MctsNodeExtensionInterface {

    public String id = "CMC";

    /**
     * Empty constructor for this extension node object
     */
    public MctsCMCNodeExtensionComposition() {
    }

    /**
     * Empty set up method as no set up is needed for this extension node object
     */
    public void setUp(MctsNodeComposition node) {
    }

    /**
     * @return the id of the player to play at the node for which this extension object relates
     */
    public String getID() {
        return id;
    }

}
