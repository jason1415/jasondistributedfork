1. Run "gradle jarFat" in the root directory.
2. Create a tournament by executing the "start_server.sh script with <port number> as command line input".

Command: ./start_server.sh \<port number>
3. There are two options for running matches:
- To run games between two enhancements execute:
	"connect_clients_tree_mcts_X_vs_Y <number of games> <first enhancement> <second enhancment>"
    The script will start the lobby as well as connect the clients.  
    It will then run <number of games> games between <first enhancment> and <second enhancment>.
    Enhancement options are: Vanilla, Fap, Contextual, Rave, Mast, Lgr
    E.g., connect_clients_tree_mcts_X_vs_Y 2 Lgr Rave 
    
    Command: ./connect_clients.sh \<number of games to run> \<enhancement for player 1> \<enhancement for player 2> \<port number> \<number of threads for both players to play on>

- To run games between two enhancements and store all output in a log file, run:
	 "run_experiment.sh <first enhancement> <second enhancments> <amount of games>". 

4. To run with a different number of threads and time per move, manually update these paramters in the GoEngine and script. 

 -- Set parameters and recompile before running matches 
   a) At lines 15 and 16 in IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/games/go/GoEngine.java
        protected int TURN_LENGTH = 3000;
    	protected int THREAD_COUNT = 4;
     These parameters are what will be used when the code is run.
   b) Run "gradle jarFat" in the root directory.  

 -- Manually update parameters for logging  
   c) At lines 1 and 2 in IngeniousFrame/scripts/go/run_experiment.sh 
	thread="4"
	turnTime="3000"   

5. To set a different number of threads for different enhancements, overwrite
	the parameters in GoEngine.java and update your scripts accordingly
        protected int TURN_LENGTH = 3000;
    	protected int THREAD_COUNT = 4;
    by adding them to IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/games/go/engines/TreeEngine/GoMCTSTree<Enhancment>Engine.java 
    e.g., add 
        protected int TURN_LENGTH = 3000;
    	protected int THREAD_COUNT = 2;
        to line 31 (inside "public class GoMCTSTreeRaveEngine extends GoEngine {...}")
        of IngeniousFrame/src/main/java/za/ac/sun/cs/ingenious/games/go/engines/TreeEngine/GoMCTSTreeRaveEngine.java 
