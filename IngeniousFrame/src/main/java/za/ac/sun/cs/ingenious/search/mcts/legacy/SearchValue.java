package za.ac.sun.cs.ingenious.search.mcts.legacy;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * This allows implementing different formulae for search values in MCTS. Most common is {@link UCT}.
 * This is used, for example, during descent with {@link MCTSDescender}. There, the nodes with the 
 * highest search values will be visited until an expandable or terminal node is reached. 
 */
public interface SearchValue<S extends TurnBasedGameState, N extends SearchNode<S,N>> {
	/**
	 * Calculates the search value of a given node. 
	 * @param node Node for which to calculate the searchValue.
	 * @param forPlayerID Player for which to calculate the search value.
	 * @param totalNofPlayouts Number of playouts that have been run before this call.
	 * @return Search value of node.
	 */
	public double get(N node, int forPlayerID, int totalNofPlayouts);
}
