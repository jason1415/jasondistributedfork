package za.ac.sun.cs.ingenious.core.commandline.client;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import za.ac.sun.cs.ingenious.core.commandline.BaseArguments;
import za.ac.sun.cs.ingenious.core.network.TCPProtocol;

/**
 * Created by chriscz on 2016/07/18.
 */
@Parameters(commandDescription = "connects to a hosted game") public class ClientArguments
        extends BaseArguments {
    @Parameter(names = "-port", description = "GameServer port [default=" + TCPProtocol.PORT + "]")
    private int serverPort = TCPProtocol.PORT;

    @Parameter(names = "-hostname", description = "GameServer hostname [default=localhost]")
    private String serverHostname = "localhost";

    @Parameter(names = "-lobby", description =
	"Name of the lobby to connect to, (will display a list "
                    + "if there's more than one possibility)")
    private String lobbyName = null;

    @Parameter(names = "-game", description =
	"The referee of the game this client wishes to play (will display a list "
			+ "if there's more than one possibility). All referees are automatically registered with their class names. You can also supply the -C command to the server script to register a referee with any name you like.")
    private String gameType = null;

    @Parameter(names = "-engine", description = "Full java identifier for the engine you wish to play with", required = true)
    private String engineClassname = null;

    @Parameter(names = "-config", description = "Path to a file containing enhancement specific configuration formatted as JSON", required = true)
    private String enhancementConfigPath = null;

    @Parameter(names = "-threadCount", description = "The number of threads to run for the current engine", required = true)
    private int threadCount = 1;

    @Parameter(names = "-username", description = "Username to connect with [default=-engine]")
    private String username = null;

    @Override public void validateAndPrepareArguments(JCommander commander) {
        if (username == null) {
            username = engineClassname;
        }
    }

    /* --- GETTERS -----------------------------------------------------------------------------*/

    public String getEnhancementConfigPath() {
        return enhancementConfigPath;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public String getEngineClassname() {
        return engineClassname;
    }

    public String getServerHostname() {
        return serverHostname;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getGameType() {
        return gameType;
    }

    public String getLobbyName() {
        return lobbyName;
    }

    public String getUsername() {
        return username;
    }
}
