package za.ac.sun.cs.ingenious.games.bomberman;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMFinalEvaluator;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMGameTerminateMessage;
import za.ac.sun.cs.ingenious.games.bomberman.network.BMInitGameMessage;

/** 
 * Base implementation of some aspects of Bomberman.  To develop an engine for the game,
 * provide an engine name without spaces, and a method for returning valid moves wrapped
 * in BMPlayMoveMessages by overriding the two abstract methods.
*/
public abstract class BMEngine extends Engine {

	protected BMBoard board;
	protected BMLogic logic = BMLogic.defaultBMLogic;
	protected BMFinalEvaluator eval = new BMFinalEvaluator();
	protected int engineNr;
	
    // TODO - clarify why we need both these constructors? See issue 149
	public BMEngine(EngineToServerConnection toServer) {
		super(toServer);
	}
	
	public BMEngine(BMBoard board, char playerID, int engineNr){
		super(null);
		this.board = board;
		this.playerID = playerID;
		this.engineNr = engineNr;
	}
	
	/**
	 * Return your engine name here, DO NOT USE SPACES!
	 */
	@Override
	public abstract String engineName();
	
	/**
	 * Overwrite this method to respond with your proposed move.
     *
     * The provided GenMoveMessage parameter contains a Clock accessible by getClock().  This clock is started when it is
     * deserialized over a socket used for communication with the referee.  One can query the
     * Clock for the remaining time before a move must be sent in.  If one does not respond with a move in
     * time, a null move is submitted by the communications handler on your behalf (see EngineToServerConnection).
     * In order to ensure you are able to deal with future move requests and prevent expired searches from using up
     * resources, it is recommended that the search itself be run on a separate thread, which is monitored and terminated when
     * the time expires, if it has not yet exited.
     * 
     * @param a Instruction to generate a move, containing a Clock object.
     * @return An instance of BMPlayMoveMessage containing the move to be played by the engine, or a null move in a PlayMoveMessage in the case of a timeout.
	 */
	@Override
	public abstract PlayActionMessage receiveGenActionMessage(GenActionMessage a);
	
	/**
	 * Called before the game starts.
     *
     * @param a Contains a representation of the initial game state, the number of players, the player ID, and the engine number.
	 */
	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		BMInitGameMessage bmAction = (BMInitGameMessage) a;
		this.playerID = bmAction.getPlayerID();
		this.engineNr = bmAction.getEngineTurn();
		this.board = new BMBoard(bmAction.getBoard(), bmAction.getNPlayers(), false, false);
	}
	
	/**
	 * Apply the provided message to the current game state.  Throws an IllegalArgumentException if the message provided
	 * is not supported by Bomberman.
	 */
	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a){
		if (!logic.applyPlayedMoveMessage(a, board)) {
			throw new IllegalArgumentException("Unexpected type of PlayMoveMessage received.");
		}
	}
	
	/**
	 * Called when the game terminates: logs the final scores.
	 */
	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a){
		Log.info("Player " + playerID + ":  game terminated received");
		double[] scores = ((BMGameTerminateMessage)a).getWinningScore();
		String s = "Scores: ";
		for (double d : scores){
			s += "|" + d;
		}
        s += "|";
        Log.info(s);
	}
}
