package za.ac.sun.cs.ingenious.games.bomberman.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.time.Clock;



public class BMGenActionMessage extends GenActionMessage {

	private static final long serialVersionUID = 1L;
	
	private int playerID;
	private int round;
	
	
	/**
	 * Creates a new {@link BMGenActionMessage}. 
	 * @param playerID the player that is supposed to make a move.
	 * @param round the current round of the board.
	 */
	public BMGenActionMessage(int playerID, int round, int timeInMS) {
		super(new Clock(timeInMS));
		this.playerID = playerID;
		this.round = round;
	}
	
	public int getPlayerID() {
		return playerID;
	}
	
	public int getRound() {
		return round;
	}
}
