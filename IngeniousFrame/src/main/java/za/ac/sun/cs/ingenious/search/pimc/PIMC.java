package za.ac.sun.cs.ingenious.search.pimc;

import com.esotericsoftware.minlog.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.PlayoutPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.TreeDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.TreeUpdater;

/**
 * Class implementing Perfect Information Monte Carlo search
 */
public final class PIMC {
	private PIMC(){}

	/**
	 * Method implementing Perfect Information Monte Carlo search. The informationSet supplied
	 * is determinized multiple times and each time, standard MCTS is used to find the best
	 * action to play for the given determinization. The results of these trails are then
	 * combined to return the best action to play regardless of determinization.
	 * 
	 * IMPORTANT: This method requires the {@link Move}s and {@link Action}s used in the game
	 * to fulfill the Object contract, i.e: equals and hashCode must be implemented correctly.
	 * 
	 * @param informationSet The current state of the game to find the best action for.
	 * @param logic Object containing the game logic.
	 * @param det Determinizer for the informationSet.
	 * @param policy Playout policy for MCTS, see also {@link MCTS}.
	 * @param descender Descender for MCTS, see also {@link MCTS}.
	 * @param updater Updater for MCTS, see also {@link MCTS}.
	 * @param sensor Object to observe the moves with during MCTS.
	 * @param searchingPlayerID ID of the player who executes the search, typically the same as
	 *            the ID of the player who has to act in informationSet.
	 * @param turnLength Time to complete the search in ms.
	 * @param numDeterminizationsToTry Number of different determinizations to run MCTS for.
	 *            Each iteration of MCTS will have turnLength/numDeterminizationsToTry ms time.
	 * @return The best action to play in the current informationSet.
	 */
	public static <S extends TurnBasedGameState> Action generateAction(S informationSet, GameLogic<S> logic,
			InformationSetDeterminizer<S> det, PlayoutPolicy<S> policy,
			TreeDescender<S,MCTSNode<S>> descender, TreeUpdater<MCTSNode<S>> updater, ActionSensor<S> sensor,
			int searchingPlayerID, long turnLength, long numDeterminizationsToTry) {

		Map<Move, Integer> visitsPerMove = new HashMap<Move, Integer>();
		
		long timePerDeterminization = turnLength/numDeterminizationsToTry;
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnLength;
		
		int numDeterminizationsTried = 0;
		while(System.currentTimeMillis() < endTime){
			numDeterminizationsTried++;
			MCTSNode<S> root = new MCTSNode<S>(
					det.determinizeUnknownInformation(informationSet, searchingPlayerID), logic, null, null);
			MCTS.generateAction(root, policy, descender, updater, timePerDeterminization);
			for (MCTSNode<S> child : root.getExpandedChildren()) {
				if (visitsPerMove.get(child.getMove())!=null) {
					visitsPerMove.put(child.getMove(), visitsPerMove.get(child.getMove()).intValue()+child.getVisits());
				} else {
					visitsPerMove.put(child.getMove(), child.getVisits());
				}
			}
		}
		
		int mostVisits = Integer.MIN_VALUE;
		Action bestAction = null;
		Log.info("PIMC", "Number of determinizations tried: " + numDeterminizationsTried);
		Log.info("PIMC", "Possible moves:");
		for (Entry<Move, Integer> e : visitsPerMove.entrySet()) {
			Log.info("PIMC", e.getKey().toString() + ", visits: " + e.getValue());
			if (!(e.getKey() instanceof Action)) {
				Log.warn("PIMC", e.getKey().toString() + " is not an Action!");
				continue;
			}
			if (e.getValue() > mostVisits) {
				mostVisits = e.getValue();
				bestAction = (Action) e.getKey();
			}
		}
		if (bestAction != null) {
			Log.debug("PIMC", "Best action found: " + bestAction.toString());
		} else {
			Log.debug("PIMC", "No best action found.");
		}
		
		return bestAction;

	}
}
