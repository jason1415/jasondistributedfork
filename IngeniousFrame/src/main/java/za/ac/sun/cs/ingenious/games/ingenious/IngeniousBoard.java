package za.ac.sun.cs.ingenious.games.ingenious;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
/**
 * Hexagonal board for Ingenious
 * 
 * @author steven
 *
 */
// TODO Fix this up! Proper seperation of gamestate and logic!!
//
public class IngeniousBoard implements IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> {

	private HashMap<Coord, Integer> gameBoard;
	public final int DIAMETER; // Diameter must be a odd number greater than or
								// equal to 3
	public final int SIDE_LENGTH;
	public final int CAPACITY;
	private IngeniousAction LAST_MOVE = null;
	private final int NUMBER_COLOURS;
	private Stack<IngeniousAction> moveHistory = new Stack<IngeniousAction>();

	/**
	 * @return int - number of colours
	 */
	@Override
	public int getNumColours(){
		return NUMBER_COLOURS;
	}
	
	public IngeniousBoard copy(){
		IngeniousBoard copy = new IngeniousBoard(DIAMETER,NUMBER_COLOURS);
		for(IngeniousAction m : this.moveHistory){
			copy.makeMove(m);
		}
		return copy;
	}
	
	public Stack<IngeniousAction> getMoveHistory(){
		return moveHistory;
	}
	
	/**
	 * Create a new Board
	 * 
	 * @throws IllegalArgumentException
	 * @param diameter
	 * @param NUMBER_COLOURS
	 */
	public IngeniousBoard(int diameter, int NUMBER_COLOURS) {
		this.NUMBER_COLOURS = NUMBER_COLOURS;
		this.DIAMETER = diameter;
		
		if(this.DIAMETER<3 || this.DIAMETER%2==0){
			throw new IllegalArgumentException("Board Diameter must be odd and greater than 3.");
		}
		
		this.SIDE_LENGTH = diameter / 2 + 1;

		this.CAPACITY = capacity(DIAMETER);
		
		gameBoard = new HashMap<Coord, Integer>();

		Coord c = new Coord(0, -SIDE_LENGTH + 1);
		gameBoard.put(c, 0);

		c = new Coord(-SIDE_LENGTH + 1, 0);
		gameBoard.put(c, 1);

		c = new Coord(SIDE_LENGTH - 1, -SIDE_LENGTH + 1);
		gameBoard.put(c, 5);

		c = new Coord(SIDE_LENGTH - 1, 0);
		gameBoard.put(c, 4);

		c = new Coord(-SIDE_LENGTH + 1, SIDE_LENGTH - 1);
		gameBoard.put(c, 2);

		c = new Coord(0, SIDE_LENGTH - 1);
		gameBoard.put(c, 3);

	}
	/**
	 * Calculates the capacity of a board with size DIAMETER
	 * 
	 * @param DIAMETER
	 * @return int capacity
	 */
	public static int capacity(int DIAMETER){
		int capacity = 0;
		int side_length = DIAMETER/2;
		
		capacity = DIAMETER*(side_length);
		
		capacity = capacity - (side_length*((side_length)+1))/2;
		
		capacity = capacity*2+DIAMETER;
		
		return capacity;
	}

	/**
	 * Generate a list of available moves based on a rack.
	 */
	public ArrayList<IngeniousAction> generateMoves(int currentPlayer, IngeniousRack rack) {
		ArrayList<IngeniousAction> moves = new ArrayList<IngeniousAction>();
		IngeniousAction nextMove;
		Coord pos;
		for (Tile tile : rack) {
			for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
				for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
					pos = new Coord(i, j);
					for (int k = 0; k < Tile.coordinateSystem.length; k++) {
						Tile temp = new Tile(tile.getTopColour(),
								tile.getBottomColour());
						temp.setRotation(k,NUMBER_COLOURS);
						nextMove = new IngeniousAction(temp, pos);
						if (validMove(nextMove)) {
							moves.add(nextMove);
						}
					}
				}
			}
		}
		//Log.info("LENGTH OF LIST SENT FROM BOARD :"+moves.size());
		return moves;
	}
	
	/**
	 * if no more tiles can be placed the board is full
	 * 
	 * @return boolean
	 */
	@Override
	public boolean full() {
		for (int i = -SIDE_LENGTH + 1; i < SIDE_LENGTH; i++) {
			for (int j = -SIDE_LENGTH + 1; j < SIDE_LENGTH; j++) {
				if (getHex(i, j) == -1) {
					Coord coord = new Coord(i, j);
					for (Coord direction : Tile.coordinateSystem) {
						Coord test = coord;
						test = test.add(direction);
						if (test.getX() < SIDE_LENGTH
								&& test.getX() > -SIDE_LENGTH) {
							if (test.getY() < SIDE_LENGTH
									&& test.getY() > -SIDE_LENGTH) {
								if (getHex(test) == -1) {
									return false;
								}
							}
						}
					}
				}
			}
		}
		return true;
	}

	/**
	 * Get the int value of the hex at (q,r)
	 * 
	 * @param q
	 * @param r
	 * @return int
	 */
	public int getHex(int q, int r) {
		Coord coord = new Coord(q, r);
		return getHex(coord);
	}

	/**
	 * Validity check for move
	 * 
	 * @return boolean
	 */
	public boolean validMove(IngeniousAction move) {
		Coord botPos = move.getPosition();
		Coord topPos = move.getTile().getTopCoord(move.getPosition());

		if ((botPos.getX() > this.DIAMETER / 2)
				|| (botPos.getX() < -this.DIAMETER / 2)) {
			return false;
		} else if ((botPos.getY() > this.DIAMETER / 2)
				|| (botPos.getY() < -this.DIAMETER / 2)) {
			return false;
		}

		if ((topPos.getX() > this.DIAMETER / 2)
				|| (topPos.getX() < -this.DIAMETER / 2)) {
			return false;
		} else if ((topPos.getY() > this.DIAMETER / 2)
				|| (topPos.getY() < -this.DIAMETER / 2)) {
			return false;
		}

		if (this.getHex(botPos) == -1) {
			if (this.getHex(topPos) == -1) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get the set of all occupied hex's
	 * 
	 * @return Set<Coordinate>
	 */
	public Set<Coord> getCoordinates() {
		return gameBoard.keySet();
	}

	/**
	 * Return string representation of the board
	 * @return String
	 */
	@Override
	public String toString() {
		String result = "";
		int prefixLength = SIDE_LENGTH - 1;
		int count = 0;
		boolean reachedMiddleLine = false;
		int k = 0;
		for (int q = -(SIDE_LENGTH - 1); q < (SIDE_LENGTH); q++) {

			for (int i = 0; i < prefixLength; i++) {
				result += " ";
			}
			if (prefixLength == 0) {
				reachedMiddleLine = true;
			}
			if (!reachedMiddleLine) {
				prefixLength--;

			} else {
				prefixLength++;
			}

			if (!reachedMiddleLine) {

				for (int r = k; r < SIDE_LENGTH; r++) {
					result += getHex(r, q);
					result += "|";
				}
				k--;
			} else {
				for (int r = -(SIDE_LENGTH - 1); r < SIDE_LENGTH - prefixLength
						+ 1; r++) {
					result += getHex(r, q);
					result += "|";
				}
			}
			count++;
			result += "\n";
		}
		return result;
	}

	/**
	 * return the last move that was made
	 * 
	 * @return Move
	 */
	@Override
	public IngeniousAction lastMove() {
		return LAST_MOVE;
	}

	/**
	 * undo the last move made
	 */
	public void undoMove() {
		gameBoard.remove(LAST_MOVE.getPosition());
		gameBoard.remove(LAST_MOVE.getTile().getTopCoord(
		LAST_MOVE.getPosition()));
		moveHistory.pop();
		if (moveHistory.size() > 0) {
			LAST_MOVE = moveHistory.peek();
		}
	}

	/**
	 * Get the int value of the hex at Coordinate
	 * 
	 * @return int
	 */
	@Override
	public int getHex(Coord position) {
		if (gameBoard.containsKey(position)) {

			return gameBoard.get(position);
		}
		return -1;
	}

	/**
	 * make move on gameboard
	 */
	public boolean makeMove(IngeniousAction move) {
		if (!validMove(move)) {
			return false;
		}

		Integer bottomColour = gameBoard.put(move.getPosition(), move.getTile()
				.getBottomColour());
		Integer topColour = gameBoard.put(
				move.getTile().getTopCoord(move.getPosition()), move.getTile()
						.getTopColour());

		if ((bottomColour == null) && (topColour == null)) {
			LAST_MOVE = move;
			moveHistory.push(LAST_MOVE);
			return true;
		}

		gameBoard.remove(move.getPosition());
		gameBoard.remove(move.getTile().getTopCoord(move.getPosition()));
		return false;
	}

	public static void main(String[] args) {
		Log.info(IngeniousBoard.capacity(3));
		IngeniousBoard board = new IngeniousBoard(7,6);
		Tile tile = new Tile(4, 4);
		tile.setRotation(4, 6);
		Coord coord = new Coord(0, 0);
		IngeniousAction move = new IngeniousAction(tile, coord);
		Log.info(board.validMove(move));
		Log.info(board.makeMove(move));
		Log.info(board);
		Log.info(board.getHex(-1, -1));
		Log.info(board.lastMove());
		Log.info(board.full());

	}

	@Override
	public boolean validMove(IngeniousGameState fromState, Move move) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean makeMove(IngeniousGameState fromState, Move move) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void undoMove(IngeniousGameState fromState, Move move) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Action> generateActions(IngeniousGameState fromState, int forPlayerID) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Action> generateActions(IngeniousGameState fromState, int forPlayerID, Action action) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isTerminal(IngeniousGameState state) {
		return full();
	}

	@Override
	public Set<Integer> getCurrentPlayersToAct(IngeniousGameState fromState) {
		// TODO Auto-generated method stub
		return null;
	}
}
