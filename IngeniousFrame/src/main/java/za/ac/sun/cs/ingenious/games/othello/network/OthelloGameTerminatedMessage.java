package za.ac.sun.cs.ingenious.games.othello.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;

/**
 * The message sent to Engines after a game has terminated. 
 *
 * @author Rudolf Stander
 *
 */
public class OthelloGameTerminatedMessage extends GameTerminatedMessage {

	/* The scores of the players at the end of the game */
	private double[] scores;

	/**
	 * Constructs a new game terminated message with the scores at the end of the
	 * game.
	 *
	 * @param scores	the players' scores at the end of the game
	 */
	public OthelloGameTerminatedMessage(double[] scores)
	{
		this.scores = scores;
	}

	/**
	 * Returns an array of the players' scores at the end of the game.
	 *
	 * @return	the scores at the end of the game
	 */
	public double[] getScores()
	{
		return scores;
	}
}

